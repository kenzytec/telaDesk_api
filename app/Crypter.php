<?php
/**
 * Created by PhpStorm.
 * User: kimadojemun
 * Date: 4/13/2018
 * Time: 5:18 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Crypter extends Model
{
    public static function decrypt($data)
    {
        $key = '394rwe78fudhwqpwriufdhr8ehyqr9pe8fud';

        $encrypter = new \Adbar\Encrypter($key);

        return $encrypter->encryptString($data['pl']);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: kimadojemun
 * Date: 4/13/2018
 * Time: 12:48 PM
 */


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Crypter;

class LoginController extends Controller
{
    public function login(Request $request){
        return Crypter::decrypt($request);
    }
}